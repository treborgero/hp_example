﻿using HP.DAL.Context.Repository;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HP.BLL;
using HP.BLL.Common;
using HP.BLL.Common.Smtp;
using HP.BLL.Security;
using HP.BLL.Security.Middleware.WindowsAuthentication;

namespace HP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            JwtSettings jwtSettings = Configuration.GetSection("Jwt").Get<JwtSettings>();
            bool isUseDeveloperExceptionPage = Configuration.GetValue<bool>("IsUseDeveloperExceptionPage");



            //Configure and require global authorization
            services.AddMvc(config =>
            {
                config.Filters.Add(new AuthorizeFilter());

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);



            //Authentication Settings
            services.AddAntiforgery(options => options.HeaderName = "X-XSRF-TOKEN");

            services.Configure<IISOptions>(o =>
            {
                o.AutomaticAuthentication = false;
                o.AuthenticationDisplayName = IISDefaults.AuthenticationScheme;
                o.ForwardClientCertificate = true;
            });

            services.AddAuthentication()
            .AddJwtBearer(options =>
            {
                jwtSettings.OnGenerateJwtSecurityToken = (claims, lastActionDate) =>
                {
                    DateTime now = DateTime.Now;

                    DateTime expires = (lastActionDate ?? now).AddMinutes(jwtSettings.ExpiresInMinutes);

                    return new JwtSecurityTokenHandler().CreateJwtSecurityToken(
                            issuer: jwtSettings.ValidIssuer,
                            audience: jwtSettings.ValidAudience,
                            subject: new ClaimsIdentity(claims),
                            notBefore: now,
                            issuedAt: now,
                            expires: expires,
                            signingCredentials: new SigningCredentials(jwtSettings.SsSignKey, SecurityAlgorithms.HmacSha256),
                            encryptingCredentials: new EncryptingCredentials(jwtSettings.SsEncryptKey, SecurityAlgorithms.Aes256KW, SecurityAlgorithms.Aes256CbcHmacSha512)
                        );
                };

                options.Events = new JwtBearerEvents()
                {

                    OnTokenValidated = (ctx) =>
                    {

                        var signInManager = ctx.HttpContext.RequestServices.GetRequiredService<ISignInManager>();

                        if (!signInManager.IsUserValid(ctx.Principal.GetId()))
                        {
                            ctx.Fail("User was updated and requires re-authentication!");
                        }

                        return Task.CompletedTask;
                    }
                };

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    AuthenticationType = JwtBearerDefaults.AuthenticationScheme,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    RequireExpirationTime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwtSettings.ValidIssuer,
                    ValidAudience = jwtSettings.ValidAudience,
                    IssuerSigningKey = jwtSettings.SsSignKey,
                    TokenDecryptionKey = jwtSettings.SsEncryptKey,
                    ClockSkew = TimeSpan.Zero
                };
            })
            .AddWindowsAuthentication(options =>
            {
                options.OnCreateClaims = (windowsPrincipal) =>
                {
                    List<Claim> claims = new List<Claim>();

                    try
                    {
                        using (var context = new PrincipalContext(ContextType.Domain))
                        {
                            var up = UserPrincipal.FindByIdentity(context, windowsPrincipal.Identity.Name);

                            claims.AddRange(new List<Claim> {
                                new Claim(ClaimTypes.AuthenticationMethod, WindowsAuthenticationDefaults.AuthenticationScheme),
                                new Claim(ClaimTypes.NameIdentifier, up.SamAccountName),
                                new Claim(ClaimTypes.GivenName, up.GivenName),
                                new Claim(ClaimTypes.Surname, up.Surname),
                                new Claim(ClaimTypes.Email, up.EmailAddress ?? ""),
                                new Claim(ClaimTypes.OtherPhone, up.VoiceTelephoneNumber ?? ""),
                                new Claim(AppClaimTypes.Description, up.Description ?? ""),
                                new Claim(AppClaimTypes.DisplayName, up.DisplayName ?? ""),
                                new Claim(AppClaimTypes.MiddleInitial, up.MiddleName?.First().ToString() ?? "")
                            });

                            //Un-comment this if there is a need to store the windows claims
                            //if (windowsPrincipal.Claims.Any())
                            //{
                            //    claims.AddRange(windowsPrincipal.Claims);
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Log();
                        claims = null;
                    }

                    return Task.FromResult(claims);
                };
            })
            .AddCertificateAuthentication(options =>
            {
                options.OnCreateClaims = (certificate, claims) =>
                {
                    //Add extra claims to the certificate principal
                    string un = claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;

                    if (!string.IsNullOrWhiteSpace(un))
                    {
                        string[] unValues = un.Split('.');
                        string lastName = "",
                        firstName = "",
                        middleName = "",
                        edipi = "";

                        if (unValues.Length == 4)
                        {
                            lastName = unValues[0];
                            firstName = unValues[1];
                            middleName = unValues[2];
                            edipi = unValues[3];
                        }
                        else if (unValues.Length == 3)
                        {
                            lastName = unValues[0];
                            firstName = unValues[1];
                            edipi = unValues[2];
                        }

                        string middleInitial = middleName?.First().ToString() ?? "";

                        claims.AddRange(new List<Claim> {
                            new Claim(ClaimTypes.Surname, lastName),
                            new Claim(ClaimTypes.GivenName, firstName),
                            new Claim(AppClaimTypes.MiddleInitial, middleInitial),
                            new Claim(AppClaimTypes.DisplayName, firstName + (!string.IsNullOrWhiteSpace(middleInitial) ? " " + middleInitial + ". " : " ") + lastName),
                        });
                    }

                    return Task.FromResult(claims);
                };
            });



            //Authorization Policies
            services.AddAuthorization(options =>
            {
                //Configure DefaultPolicy:

                //Set JWT authentication as default
                var policy = new AuthorizationPolicyBuilder(
                    JwtBearerDefaults.AuthenticationScheme
                  );

                //Add ip, user-agent, and token persistent security check for identity
                if (jwtSettings.IsDatabasePersistent || jwtSettings.IsUseIpAddressValidation || jwtSettings.IsUseUserAgentValidation)
                {
                    policy = policy.AddRequirements(
                        new CheckAuthRequirement
                        {
                            IpClaimType = AppClaimTypes.IpAddress,
                            UserAgentClaimType = AppClaimTypes.UserAgent,
                            AuthenticationType = JwtBearerDefaults.AuthenticationScheme
                        });
                }

                //Require authentication for all users
                policy = policy.RequireAuthenticatedUser();

                options.DefaultPolicy = policy.Build();
            });



            //Enable CORS
            services.AddCors();



            //Dependency Injection
            services.AddMemoryCache();
            services.AddHttpContextAccessor();
            services.AddSingleton(Configuration.GetSection("SignIn").Get<SignInSettings>());
            services.AddSingleton(Configuration.GetSection("Authentication").Get<AuthenticationSettings>());
            services.AddSingleton(jwtSettings);

            //SMTP
            var smtpSettings = Configuration.GetSection("SmtpMailServer").Get<SmtpMailServerSettings>();

            services.AddSingleton(smtpSettings);
            services.AddSingleton<ISmtpSender, SmtpSender>();
            if (smtpSettings.IsEnabled)
                services.AddHostedService<SmtpSenderHosterService>();
            //END SMTP

            services.AddScoped<IAppLogger, AppLogger>();
            services.AddScoped<IErrorRepository, ErrorRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserLoginRepository, UserLoginRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IUserClaimRepository, UserClaimRepository>();
            services.AddScoped<IUserTokenRepository, UserTokenRepository>();
            services.AddScoped<IContactRepository, ContactRepository>();
            services.AddScoped<IAuthorizationHandler, CheckAuthHandler>(); //Add ip security check for identity claims
            services.AddScoped<ISignInManager, SignInManager>();
            services.AddScoped<IFileInfoRepository, FileInfoRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IAntiforgery antiforgery, IAppLogger logger)
        {
            if (env.IsDevelopment() || Configuration.GetValue<bool>("IsUseDeveloperExceptionPage"))
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //Set error logging event
            Globals.SetOnErrorEvent((e) =>
            {
                logger.Log(e);
            });


            //Set global error logging middleware
            app.Use(next => async context =>
            {
                try
                {
                    await next(context);
                }
                catch (Exception e)
                {
                    try
                    {
                        (context.RequestServices.GetRequiredService<IAppLogger>()).LogByStatusCode(e, context.Response?.StatusCode ?? 0);
                    }
                    catch (Exception)
                    {
                    }

                    throw e;
                }

            });

            app.UseHttpsRedirection();

            app.UseCors(x => x
                .WithOrigins(Configuration.GetValue<string>("AllowedHosts").Split(","))
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                    ForwardedHeaders.XForwardedProto
            });

            //Custom middleware for query-string token authentication
            app.Use(next => context =>
            {
                string path = context.Request.Path.Value;

                if (
                    path.Equals("/api/file", StringComparison.OrdinalIgnoreCase)
                )
                {
                    var token = context.Request.Query.FirstOrDefault(q => q.Key == "token");

                    if (!token.Equals(default(KeyValuePair<string, Microsoft.Extensions.Primitives.StringValues>)))
                    {
                        context.Request.Headers.Add("Authorization", "Bearer " + token.Value);
                    }
                }

                return next(context);
            });

            app.UseAuthentication();

            //Custom middleware for Angular Antiforgery header tokens
            //app.Use(next => context =>
            //{
            //    string path = context.Request.Path.Value;

            //    if (
            //           path.Equals("/", StringComparison.OrdinalIgnoreCase)
            //        || path.Equals("/index.html", StringComparison.OrdinalIgnoreCase)
            //    )
            //    {
            //        context.Response.OnStarting(() =>
            //        {
            //            if (context.Response.StatusCode != StatusCodes.Status401Unauthorized)
            //            {
            //                // The request token can be sent as a JavaScript-readable cookie, 
            //                // and Angular uses it by default.
            //                var tokens = antiforgery.GetAndStoreTokens(context);
            //                context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken,
            //                    new CookieOptions() { HttpOnly = false });
            //            }

            //            return Task.CompletedTask;
            //        });
            //    }

            //    return next(context);
            //});

            app.UseMvc();
        }
    }
}
