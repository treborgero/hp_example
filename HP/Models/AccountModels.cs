﻿using System.ComponentModel.DataAnnotations;

namespace HP.Models
{
    public class LoginModel
    {
        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string FirstName { get; set; }

        [StringLength(1)]
        public string MiddleInitial { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string LastName { get; set; }

        [Required]
        [StringLength(20)]
        public string Title { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(256)]
        public string Email { get; set; }

        [Phone]
        [StringLength(50)]
        public string PhoneNumber { get; set; }


        private string _DisplayName;

        [StringLength(100)]
        public string DisplayName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this._DisplayName))
                {
                    return (string.IsNullOrWhiteSpace(this.Title) ? "" : this.Title + " ")
                        + this.FirstName
                        + (string.IsNullOrWhiteSpace(this.MiddleInitial) ? " " : " " + this.MiddleInitial + " ")
                        + this.LastName;
                }
                else
                {
                    return this._DisplayName;
                }
            }
            set { _DisplayName = value; }
        }
    }

    public class RegisterFormModel : RegisterModel
    {
        [Required]
        [StringLength(50, MinimumLength = 4)]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [Display(Name = "Password")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The passwords do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public enum RegisterResult
    {
        Success,
        Failed,
        AlreadyFound
    }
}
