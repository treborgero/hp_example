﻿using HP.DAL.Context.Entities;
using HP.DAL.Context.Repository;
using HP.Models;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NetTools;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using HP.BLL.Common;
using HP.BLL.Common.Smtp;
using HP.BLL.Security;
using HP.BLL.Security.Middleware.CertificateAuthentication;
using HP.BLL.Security.Middleware.WindowsAuthentication;
using SignInResult = HP.BLL.Security.SignInResult;

namespace HP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        public AccountController(
            IConfiguration configuration,
            IUserRepository userService,
            ISignInManager signInManager,
            SignInSettings signInSettings,
            AuthenticationSettings authenticationSettings,
            IAntiforgery antiforgery,
            ISmtpSender smtpSender
            )
        {
            _configuration = configuration;
            _svcUser = userService;
            _mgrSignIn = signInManager;
            _SignInSettings = signInSettings;
            _AuthenticationSettings = authenticationSettings;
            _Antiforgery = antiforgery;
            _smtpSender = smtpSender;
        }


        ISmtpSender _smtpSender { get; }
        IConfiguration _configuration { get; }
        IUserRepository _svcUser { get; }
        ISignInManager _mgrSignIn { get; }
        SignInSettings _SignInSettings { get; }
        IAntiforgery _Antiforgery { get; }
        AuthenticationSettings _AuthenticationSettings { get; }


        // POST: api/Account
        [HttpPost("Login")]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<SignInResult>> Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Credentials");
            }
            else if (_AuthenticationSettings.IsFormsEnabled)
            {
                var result = await _mgrSignIn.PasswordSignInAsync(
                    model.UserName,
                    model.Password,
                    LoginProviderType.Forms);

                return result;
            }
            else
            {
                return Unauthorized();
            }
        }


        [HttpPost("Login/Windows")]
        [Authorize(AuthenticationSchemes = WindowsAuthenticationDefaults.AuthenticationScheme)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<SignInResult>> LoginWindows()
        {
            if (!_AuthenticationSettings.IsWindowsEnabled)
            {
                return Unauthorized();
            }


            return await LoginByProviderType(LoginProviderType.Windows);
        }


        [HttpPost("Login/Pki")]
        [Authorize(AuthenticationSchemes = CertificateAuthenticationDefaults.AuthenticationScheme)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<SignInResult>> LoginPki()
        {
            if (!_AuthenticationSettings.IsPkiEnabled)
            {
                return Unauthorized();
            }

            return await LoginByProviderType(LoginProviderType.PKI);
        }


        [HttpGet("Settings")]
        [AllowAnonymous]
        public ActionResult<AuthenticationSettings> Settings()
        {
            AuthenticationSettings settings = new AuthenticationSettings
            {
                IsFormsEnabled = _AuthenticationSettings.IsFormsEnabled,
                IsWindowsEnabled = _AuthenticationSettings.IsWindowsEnabled,
                IsPkiEnabled = _AuthenticationSettings.IsPkiEnabled,
                IsAutoAuthenticate = _AuthenticationSettings.IsAutoAuthenticate,
                XsrfToken = _Antiforgery.GetAndStoreTokens(HttpContext).RequestToken
            };


            bool isUseDeveloperExceptionPage = _configuration.GetValue<bool>("IsUseDeveloperExceptionPage");


            if (_AuthenticationSettings.IsAutoAuthenticate || isUseDeveloperExceptionPage)
            {
                string clientIp = HttpContext.Connection.RemoteIpAddress?.ToString();

                if (!string.IsNullOrWhiteSpace(clientIp))
                {
                    if (_AuthenticationSettings.ExternalIpAddresses?.Count > 0)
                    {
                        foreach (var ipExternal in _AuthenticationSettings.ExternalIpAddresses)
                        {
                            if (ipExternal.Contains("/") || ipExternal.Contains("-"))
                            {
                                if (IPAddressRange.TryParse(ipExternal, out IPAddressRange range))
                                {
                                    if (range.Contains(System.Net.IPAddress.Parse(clientIp)))
                                    {
                                        settings.IsExternalClient = true;
                                    }
                                }
                            }
                            else if (ipExternal == clientIp)
                            {
                                settings.IsExternalClient = true;
                            }
                        }
                    }
                    else if (_AuthenticationSettings.InternalIpAddresses?.Count > 0)
                    {
                        foreach (var ipInternal in _AuthenticationSettings.InternalIpAddresses)
                        {
                            if (ipInternal.Contains("/") || ipInternal.Contains("-"))
                            {
                                if (IPAddressRange.TryParse(ipInternal, out IPAddressRange range))
                                {
                                    if (!range.Contains(System.Net.IPAddress.Parse(clientIp)))
                                    {
                                        settings.IsExternalClient = true;
                                    }
                                }
                            }
                            else if (ipInternal != clientIp)
                            {
                                settings.IsExternalClient = true;
                            }
                        }
                    }
                }

                //For debug purposes
                if (isUseDeveloperExceptionPage)
                {
                    new Exception("Client is loading authentication settings with IP address: " + clientIp ?? "").Log();
                }
            }


            return settings;
        }


        [HttpPost("Register")]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<RegisterResult>> Register(RegisterFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (_AuthenticationSettings.IsFormsEnabled)
            {
                return await RegisterByProviderType(model, LoginProviderType.Forms);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost("Register/Windows")]
        [Authorize(AuthenticationSchemes = WindowsAuthenticationDefaults.AuthenticationScheme)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<RegisterResult>> RegisterWindows(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (_AuthenticationSettings.IsWindowsEnabled)
            {
                return await RegisterByProviderType(model, LoginProviderType.Windows);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost("Register/Pki")]
        [Authorize(AuthenticationSchemes = CertificateAuthenticationDefaults.AuthenticationScheme)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<RegisterResult>> RegisterPki(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (_AuthenticationSettings.IsPkiEnabled)
            {
                return await RegisterByProviderType(model, LoginProviderType.PKI);
            }
            else
            {
                return Unauthorized();
            }
        }


        [HttpPost("RefreshToken")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<SignInResult>> RefreshToken(RefreshTokenModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                return await _mgrSignIn.SignInWithRefreshToken(model);
            }
        }


        private async Task<ActionResult<SignInResult>> LoginByProviderType(LoginProviderType type)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized();
            }
            else
            {
                var userDto = new HP.DAL.Context.Entities.User
                {
                    UserName = User.GetNameIdentifier()
                };


                //Try to sign in
                var result = await _mgrSignIn.SignInAsync(
                    userDto.UserName,
                    type);


                //If not found and is auto register then create the user and sign in.
                if (result.Status == SignInStatus.NotFound)
                {
                    RegisterModel model = new RegisterModel
                    {
                        DisplayName = User.GetDisplayName(),
                        FirstName = User.GetFirstName(),
                        MiddleInitial = User.GetMiddleInitial(),
                        LastName = User.GetLastName(),
                        Email = User.GetEmail() ?? "",
                    };

                    if (_SignInSettings.IsAutoRegister)
                    {
                        if (await RegisterByProviderType(model, type) == RegisterResult.Success)
                        {
                            result = await _mgrSignIn.SignInAsync(
                            userDto.UserName,
                            type);
                        }
                    }
                    else
                    {
                        result.Claims = new List<SignInClaim>
                        {
                            new SignInClaim() { Type = "Registration", Value = JsonConvert.SerializeObject(model, new JsonSerializerSettings(){ ContractResolver = new CamelCasePropertyNamesContractResolver() }) }
                        };
                    }
                }


                return result;
            }

        }

        private async Task<RegisterResult> RegisterByProviderType(RegisterModel model, LoginProviderType type)
        {

            var userDto = new HP.DAL.Context.Entities.User();

            //Set username and password
            if (type == LoginProviderType.Forms)
            {
                userDto.UserName = (model as RegisterFormModel).UserName;
                userDto.Password = (model as RegisterFormModel).Password;

                //Hash the password
                userDto.Password = CryptoService.Hash(userDto.Password);
            }
            else
            {
                userDto.UserName = User.GetNameIdentifier();
            }


            //Fail safe when user already exists.
            var userResult = await _svcUser.GetByUserName(userDto.UserName);

            if (userResult != null)
            {
                return RegisterResult.AlreadyFound;
            }


            //Set properties
            userDto.FirstName = model.FirstName;
            userDto.MiddleInitial = model.MiddleInitial;
            userDto.LastName = model.LastName;
            userDto.Title = model.Title;
            userDto.Email = model.Email ?? "";
            userDto.Phonenumber = model.PhoneNumber ?? "";
            userDto.DisplayName = model.DisplayName;
            userDto.IsApproved = _SignInSettings.IsApprovalRequired ? false : true;


            //Set Claims
            List<Claim> userClaims = null;

            if (type == LoginProviderType.Forms)
            {
                userClaims = new List<Claim>();
                userClaims.AddRange(new List<Claim> {
                                new Claim(ClaimTypes.AuthenticationMethod, "Forms"),
                                new Claim(ClaimTypes.NameIdentifier, userDto.UserName),
                                new Claim(ClaimTypes.GivenName, userDto.FirstName),
                                new Claim(ClaimTypes.Surname, userDto.LastName),
                                new Claim(ClaimTypes.Email, userDto.Email),
                                new Claim(ClaimTypes.OtherPhone, userDto.Phonenumber),
                                new Claim(AppClaimTypes.DisplayName, userDto.DisplayName),
                                new Claim(AppClaimTypes.MiddleInitial, model.MiddleInitial ?? "")
                            });
            }
            else
            {
                userClaims = new List<Claim>(User.Claims);
            }

#if DEBUG
            userClaims.Add(new Claim(ClaimTypes.Role, "Administrator"));
            userDto.IsApproved = true;
            userDto.ApprovedBy = -1;
            userDto.ApprovedDate = DateTime.Now;
#endif

            //Create user
            var insertResult = await _svcUser.Insert(userDto, userClaims, type);

            if (insertResult > 0)
            {
                var result = insertResult > 0 ? RegisterResult.Success : RegisterResult.Failed;

                if (result == RegisterResult.Success)
                {
                    await SendUserVerificationRequiredEmail(userDto);
                }

                return result;
            }
            else
            {
                return RegisterResult.Failed;
            }

        }

        private async Task SendUserVerificationRequiredEmail(HP.DAL.Context.Entities.User userDto)
        {
            if (_smtpSender.IsEnabled && userDto.IsApproved.HasValue && userDto.IsApproved.Value == false)
            {
                var toEmails = await _svcUser.GetEmailsByRoles("Administrator");

                if (toEmails.Any())
                {
                    string domainName = Request.Scheme + "://" + (Request.Host.Value ?? "") + (Request.PathBase.Value ?? "");

                    StringBuilder bodyBuilder = new StringBuilder();

                    bodyBuilder.Append("The following user requires account approval:");
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("Title: " + userDto.Title ?? "");
                    bodyBuilder.AppendLine("Name: " + userDto.FirstName
                        + (string.IsNullOrWhiteSpace(userDto.MiddleInitial) ? " " : " " + userDto.MiddleInitial + " ")
                        + userDto.LastName);
                    bodyBuilder.AppendLine("Email: " + userDto.Email);
                    bodyBuilder.AppendLine("Phone: " + userDto.Phonenumber);
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("Harmony Pay | " + domainName);

                    _smtpSender.Queue(new SmtpSenderMessage()
                    {
                        Subject = "Harmony Pay: User Approval Required",
                        Body = bodyBuilder.ToString(),
                        ToAddresses = toEmails
                    });
                }

            }

        }
    }
}
