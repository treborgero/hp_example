﻿using HP.BLL.Common.Smtp;
using HP.BLL.Security;
using HP.DAL.Context.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator")]
    public class UserController : ControllerBase
    {
        private readonly ISmtpSender _smtpSender;
        private readonly IUserRepository _svcUser;
        private readonly IUserClaimRepository _svcUserClaim;
        private readonly IRoleRepository _svcRole;
        private readonly ISignInManager _mgrSignIn;

        public UserController(
            IUserRepository uService,
            IUserClaimRepository userClaimService,
            IRoleRepository roleService,
            ISmtpSender smtpSender,
            ISignInManager signInManager
        )
        {
            _svcUser = uService;
            _svcUserClaim = userClaimService;
            _svcRole = roleService;
            _smtpSender = smtpSender;
            _mgrSignIn = signInManager;
        }

        // GET: api/Platform
        [HttpGet]
        public async Task<IEnumerable<HP.DAL.Context.Entities.User>> Get()
        {
            var result = await _svcUser.GetAll();

            return result;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<DAL.Context.Entities.User>> Get(int id)
        {
            var result = await _svcUser.GetById(id);

            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return result;
            }
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<bool>> Put(int id, [FromBody] DAL.Context.Entities.User model)
        {
            if (id <= 0 || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                var currentUserResult = await _svcUser.GetById(id);

                if (currentUserResult == null)
                {
                    return NotFound();
                }


                string displayName = (string.IsNullOrWhiteSpace(model.Title) ? "" : model.Title + " ")
                        + model.FirstName
                        + (string.IsNullOrWhiteSpace(model.MiddleInitial) ? " " : " " + model.MiddleInitial + " ")
                        + model.LastName;


                bool isNowApproved = (model.IsApproved ?? false) && !(currentUserResult.IsApproved ?? false);
                bool isNowNotApproved = !(model.IsApproved ?? false) && (currentUserResult.IsApproved ?? false);
                bool isNowEnabled = !(model.IsDisabled) && (currentUserResult.IsDisabled);
                bool isNowDisabled = (model.IsDisabled) && !(currentUserResult.IsDisabled);


                string roles = (model.Roles != currentUserResult.Roles) ? model.Roles : null;


                var result = await _svcUser.Update(id, new
                {//HP.DAL.Context.Entities.User
                    FirstName = model.FirstName,
                    MiddleInitial = model.MiddleInitial,
                    LastName = model.LastName,
                    Title = model.Title,
                    DisplayName = displayName,
                    Email = model.Email,
                    Phonenumber = model.Phonenumber ?? "",
                    IsApproved = model.IsApproved,
                    IsDisabled = model.IsDisabled,
                    ApprovedBy = (int?)(isNowApproved ? (object)User.GetId() : (isNowNotApproved ? null : model.ApprovedBy)),
                    ApprovedDate = (DateTime?)(isNowApproved ? (object)DateTime.Now : (isNowNotApproved ? null : model.ApprovedDate)),
                    DisabledBy = (int?)(isNowDisabled ? (object)User.GetId() : (isNowEnabled ? null : model.DisabledBy)),
                    DisabledDate = (DateTime?)(isNowDisabled ? (object)DateTime.Now : (isNowEnabled ? null : model.DisabledDate))
                }, roles);


                if (result > 0)
                {
                    _mgrSignIn.InvalidateUser(currentUserResult.Id);

                    if (isNowApproved)
                    {
                        await SendUserApprovedEmail(model);
                    }
                    if (isNowEnabled)
                    {
                        await SendUserEnabledEmail(model);
                    }
                }


                return (result > 0);
            }
        }

        [HttpGet("Lookups/Roles")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IEnumerable<DAL.Context.Entities.Role>> GetRoleLookups()
        {
            var result = await _svcRole.GetAll();

            return result;
        }

        [HttpPost("{id:int}/SendTestEmail")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<bool>> SendTestEmail(int id)
        {
            if (id <= 0)
            {
                return BadRequest(ModelState);
            }
            else
            {
                var result = await _svcUser.GetById(id);

                if (result == null)
                {
                    return NotFound();
                }

                var userDto = result;

                if (_smtpSender.IsEnabled && !string.IsNullOrWhiteSpace(userDto.Email))
                {
                    var ccEmails = await _svcUser.GetEmailsByRoles("Administrator");

                    string domainName = Request.Scheme + "://" + (Request.Host.Value ?? "") + (Request.PathBase.Value ?? "");

                    StringBuilder bodyBuilder = new StringBuilder();

                    bodyBuilder.Append("This is a notification test:");
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("Title: " + userDto.Title ?? "");
                    bodyBuilder.AppendLine("Name: " + userDto.FirstName
                        + (string.IsNullOrWhiteSpace(userDto.MiddleInitial) ? " " : " " + userDto.MiddleInitial + " ")
                        + userDto.LastName);
                    bodyBuilder.AppendLine("Email: " + userDto.Email);
                    bodyBuilder.AppendLine("Phone: " + userDto.Phonenumber ?? "");
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("");
                    bodyBuilder.AppendLine("Harmony Pay | " + domainName);

                    _smtpSender.Queue(new SmtpSenderMessage()
                    {
                        Subject = "HP: Notification Test!",
                        Body = bodyBuilder.ToString(),
                        ToAddresses = new List<string>() { userDto.Email },
                        CcAddresses = ccEmails
                    });

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private async Task SendUserApprovedEmail(HP.DAL.Context.Entities.User userDto)
        {
            if (_smtpSender.IsEnabled && userDto.IsApproved.HasValue && userDto.IsApproved.Value == true && !string.IsNullOrWhiteSpace(userDto.Email))
            {
                var ccEmails = await _svcUser.GetEmailsByRoles("Administrator");

                string domainName = Request.Scheme + "://" + (Request.Host.Value ?? "") + (Request.PathBase.Value ?? "");

                StringBuilder bodyBuilder = new StringBuilder();

                bodyBuilder.Append("The following user account is now approved for login:");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("Title: " + userDto.Title ?? "");
                bodyBuilder.AppendLine("Name: " + userDto.FirstName
                    + (string.IsNullOrWhiteSpace(userDto.MiddleInitial) ? " " : " " + userDto.MiddleInitial + " ")
                    + userDto.LastName);
                bodyBuilder.AppendLine("Email: " + userDto.Email);
                bodyBuilder.AppendLine("Phone: " + userDto.Phonenumber ?? "");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("Harmony Pay | " + domainName);

                _smtpSender.Queue(new SmtpSenderMessage()
                {
                    Subject = "HP: User Approved",
                    Body = bodyBuilder.ToString(),
                    ToAddresses = new List<string>() { userDto.Email },
                    CcAddresses = ccEmails
                });
            }

        }

        private async Task SendUserEnabledEmail(HP.DAL.Context.Entities.User userDto)
        {
            if (_smtpSender.IsEnabled && !userDto.IsDisabled && !string.IsNullOrWhiteSpace(userDto.Email))
            {
                var ccEmails = await _svcUser.GetEmailsByRoles("Administrator");

                string domainName = Request.Scheme + "://" + (Request.Host.Value ?? "") + (Request.PathBase.Value ?? "");

                StringBuilder bodyBuilder = new StringBuilder();

                bodyBuilder.Append("The following user account is now enabled for login:");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("Title: " + userDto.Title ?? "");
                bodyBuilder.AppendLine("Name: " + userDto.FirstName
                    + (string.IsNullOrWhiteSpace(userDto.MiddleInitial) ? " " : " " + userDto.MiddleInitial + " ")
                    + userDto.LastName);
                bodyBuilder.AppendLine("Email: " + userDto.Email);
                bodyBuilder.AppendLine("Phone: " + userDto.Phonenumber ?? "");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("");
                bodyBuilder.AppendLine("Harmony Pay | " + domainName);

                _smtpSender.Queue(new SmtpSenderMessage()
                {
                    Subject = "HP: User Enabled",
                    Body = bodyBuilder.ToString(),
                    ToAddresses = new List<string>() { userDto.Email },
                    CcAddresses = ccEmails
                });
            }

        }
    }
}