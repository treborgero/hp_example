﻿using HP.DAL.Context.Entities;
using HP.DAL.Context.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IContactRepository _svcContact;

        public ContactController(IContactRepository contactService)
        {
            _svcContact = contactService;
        }

        // GET: api/Contact
        [HttpGet]
        [AllowAnonymous]
        [ResponseCache(Duration = 1200, Location = ResponseCacheLocation.Any)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Contact>>> Get()
        {
            var data = await _svcContact.GetAll();
           
            return Ok(data);
        }
    }
}
