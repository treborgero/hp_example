﻿using HP.DAL.Context.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ErrorController : ControllerBase
    {
        private readonly IErrorRepository _svcError;

        public ErrorController(IErrorRepository eService)
        {
            _svcError = eService;
        }

        // GET: api/Error
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<IEnumerable<DAL.Context.Entities.Error>> Get()
        {
            //Gets latest 500 errors
            var data = await _svcError.GetAll();

            return data;
        }

    }
}