﻿using HP.DAL.Context.Entities;
using HP.DAL.Context.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using HP.BLL.Common;

namespace HP.BLL.Security
{
    public class SignInManager : ISignInManager
    {
        public SignInManager(
            IUserRepository userService,
            IUserClaimRepository userClaimService,
            IUserLoginRepository userLoginService,
            IUserTokenRepository userTokenService,
            IHttpContextAccessor httpContextAccessor,
            SignInSettings settings,
            JwtSettings jwtSettings,
            IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _svcUser = userService;
            _svcUserToken = userTokenService;
            _svcUserClaim = userClaimService;
            _svcUserLogin = userLoginService;
            _SignInSettings = settings;
            _JwtSettings = jwtSettings;

            _ClientIpAddress = httpContextAccessor.HttpContext.Connection.RemoteIpAddress?.ToString();

            if (httpContextAccessor.HttpContext.Request.Headers.TryGetValue("User-Agent", out StringValues userAgentValue))
            {
                _ClientUserAgent = userAgentValue.ToString();
            }
        }


        IMemoryCache _cache { get; }
        IUserRepository _svcUser { get; }
        IUserTokenRepository _svcUserToken { get; }
        IUserClaimRepository _svcUserClaim { get; }
        IUserLoginRepository _svcUserLogin { get; }
        SignInSettings _SignInSettings { get; }
        JwtSettings _JwtSettings { get; }
        string _ClientIpAddress { get; }
        string _ClientUserAgent { get; }



        internal async Task<SignInResult> PasswordSignInAsync(
            string userName,
            string password,
            LoginProviderType providerType,
            RefreshTokenModel refreshModel)
        {
            SignInResult result = new SignInResult
            {
                Claims = new List<SignInClaim>(),
                Status = SignInStatus.Failure
            };

            var userResult = await _svcUser.GetByUserName(userName);

            if (userResult != null)
            {
                var user = userResult;

                _cache.Remove(user.Id.ToString());

                if (!user.IsVerified)
                {
                    result.Status = SignInStatus.RequiresVerification;
                }
                else if (user.IsApproved.HasValue && user.IsApproved.Value == false)
                {
                    result.Status = SignInStatus.RequiresApproval;
                }
                else if (user.IsDisabled)
                {
                    result.Status = SignInStatus.Disabled;
                }
                else if (_SignInSettings.IsLockoutEnabled && user.IsLocked)
                {
                    result.Status = SignInStatus.LockedOut;
                    result.LockoutEndDate = user.LockoutEndDate;
                }
                else
                {
                    if (_SignInSettings.IsLockoutEnabled && user.LockoutEndDate.HasValue)
                    {
                        user.LockoutEndDate = null;
                        user.FailedLoginCount = 0;
                        await _svcUser.UpdateLockoutInfo(user);
                    }


                    bool isPasswordMatch = refreshModel != null ? true : ((providerType == LoginProviderType.Forms) ? CryptoService.Verify(password, user.Password) : false);


                    if (providerType == LoginProviderType.Forms && _SignInSettings.IsLockoutEnabled && !isPasswordMatch)
                    {
                        user.LockoutEndDate = null;
                        user.FailedLoginCount++;

                        if (user.FailedLoginCount >= _SignInSettings.MaxAccessFailCount)
                        {
                            user.LockoutEndDate = DateTime.Now.AddMinutes(_SignInSettings.LockoutTimespanInMinutes);

                            result.Status = SignInStatus.LockedOut;
                            result.LockoutEndDate = user.LockoutEndDate;
                        }

                        await _svcUser.UpdateLockoutInfo(user);
                    }
                    else if (providerType != LoginProviderType.Forms || isPasswordMatch)
                    {
                        //Set Claims      
                        IEnumerable<Claim> claims = await GenerateClaimsForUser(user, providerType);

                        if (claims.Any())
                        {
                            //Set result
                            result.Token = GenerateJsonWebToken(claims, _JwtSettings, out DateTime tokenExpirationTimeUtc, (refreshModel?.LastActionDate));
                            result.TokenExpirationTime = tokenExpirationTimeUtc;


                            //Store Token if enabled
                            if (_JwtSettings.IsDatabasePersistent)
                            {
                                var tResult = await _svcUserToken.Insert(new
                                {
                                    UserId = user.Id,
                                    IpAddress = _ClientIpAddress,
                                    UserAgent = _ClientUserAgent,
                                    Token = result.Token,
                                    //Set ExpirationDate to be longer in the database record!
                                    ExpirationDate = result.TokenExpirationTime.Value.AddMinutes(_JwtSettings.ExpiresInMinutes),
                                    TokenType = UserTokenType.Token
                                });

                                if (tResult == 0)
                                {
                                    result.Token = null;
                                    result.TokenExpirationTime = null;
                                }
                                else if (refreshModel?.UserTokenId.HasValue ?? false)
                                {
                                    await _svcUserToken.Delete(refreshModel.UserTokenId.Value);
                                }
                            }


                            //Set Refresh Token
                            if (result.Token != null && _JwtSettings.IsUseRefreshTokens)
                            {
                                string newRefreshToken = GenerateRefreshToken();
                                DateTime refreshTokenExpirationTime = result.TokenExpirationTime.Value.AddMinutes(_JwtSettings.ExpiresInMinutes < 5 ? 5 : _JwtSettings.ExpiresInMinutes);

                                int rtResult = 0;

                                if (refreshModel == null)
                                {   //Set new Refresh Token
                                    rtResult = await _svcUserToken.Insert(new
                                    {
                                        UserId = user.Id,
                                        IpAddress = _ClientIpAddress,
                                        UserAgent = _ClientUserAgent,
                                        Token = newRefreshToken,
                                        ExpirationDate = refreshTokenExpirationTime,
                                        TokenType = UserTokenType.RefreshToken
                                    });
                                }
                                else
                                {
                                    //Update Refresh Token
                                    var currentRefreshTokenResult = await _svcUserToken.Get(user.Id, refreshModel.RefreshToken);

                                    if (currentRefreshTokenResult != null)
                                    {
                                        if (currentRefreshTokenResult.RefreshCount < _JwtSettings.RefreshTokenMaxProvisionCount)
                                        {
                                            currentRefreshTokenResult.RefreshCount++;

                                            rtResult = await _svcUserToken.Update(new
                                            {
                                                Token = newRefreshToken,
                                                RefreshCount = currentRefreshTokenResult.RefreshCount,
                                                ExpirationDate = refreshTokenExpirationTime,
                                                UpdatedDate = DateTime.Now
                                            }, "id_user_token=@id_user_token", new { id_user_token = currentRefreshTokenResult.Id });
                                        }
                                    }
                                }


                                if (rtResult == 0)
                                {
                                    result.Token = null;
                                    result.TokenExpirationTime = null;
                                }
                                else
                                {
                                    result.RefreshToken = newRefreshToken;
                                    result.RefreshTokenExpirationTime = refreshTokenExpirationTime;
                                }
                            }


                            //Delete Expired tokens
                            if (_JwtSettings.IsDatabasePersistent || _JwtSettings.IsUseRefreshTokens)
                            {
                                await _svcUserToken.DeleteExpired(_JwtSettings.RefreshTokenMaxProvisionCount);
                            }


                            //Success
                            if (result.Token != null)
                            {
                                //Update LockoutInfo if forms 
                                if (providerType == LoginProviderType.Forms)
                                {
                                    user.LockoutEndDate = null;
                                    user.FailedLoginCount = 0;

                                    await _svcUser.UpdateLockoutInfo(user);
                                }

                                //Set public claims
                                result.Claims = new List<SignInClaim>(claims.Where(c =>
                                       c.Type != ClaimTypes.Name
                                ).Select(c => new SignInClaim { Type = c.Type, Value = c.Value }));

                                result.Status = SignInStatus.Success;
                            }
                        }
                    }
                }



                //Get login provider for this user and log the sign in.
                var loginProvider = await _svcUser.GetLoginProvider(user.Id, providerType);

                if (loginProvider != null)
                {
                    await _svcUserLogin.Insert(new
                    {
                        UserId = user.Id,
                        IpAddress = _ClientIpAddress ?? "",
                        LoginStatusId = Enum.Parse(typeof(SignInStatusId), Enum.GetName(typeof(SignInStatus), result.Status)),
                        UserLoginProviderId = loginProvider.Id
                    });
                }
            }
            else if (userResult == null && providerType != LoginProviderType.Forms)
            {
                result.Status = SignInStatus.NotFound;
            }

            return result;
        }

        public async Task<SignInResult> PasswordSignInAsync(
            string userName,
            string password,
            LoginProviderType providerType)
        {
            return await PasswordSignInAsync(userName, password, providerType, null);
        }

        public async Task<SignInResult> SignInAsync(
            string userName,
            LoginProviderType providerType,
            RefreshTokenModel refreshModel)
        {
            return await PasswordSignInAsync(userName, null, providerType, refreshModel);
        }

        public async Task<SignInResult> SignInAsync(
            string userName,
            LoginProviderType providerType)
        {
            return await PasswordSignInAsync(userName, null, providerType, null);
        }

        public void InvalidateUser(int userId)
        {

            int minutes = _JwtSettings.ExpiresInMinutes * _JwtSettings.RefreshTokenMaxProvisionCount;

            if (minutes <= 0)
            {
                minutes = 20;
            }

            _cache.Set(userId.ToString(), userId, TimeSpan.FromMinutes(minutes));
        }

        public bool IsUserValid(int userId)
        {
            return !_cache.TryGetValue(userId.ToString(), out int value);
        }

        public async Task<SignInResult> SignInWithRefreshToken(RefreshTokenModel model)
        {
            SignInResult result = new SignInResult
            {
                Claims = new List<SignInClaim>(),
                Status = SignInStatus.Failure
            };


            SecurityToken validatedToken = null;
            ClaimsPrincipal validatedUser = null;

            try
            {
                validatedUser = ValidateToken(model.Token, out validatedToken);
            }
            catch (Exception)
            {

            }

            if (!_JwtSettings.IsUseRefreshTokens || validatedToken == null || validatedUser == null)
            {
                return result;
            }


            int userId = validatedUser.GetId();

            if (userId > 0 && IsUserValid(userId))
            {
                bool isValidToken = true;
                model.UserTokenId = null;


                if (_JwtSettings.IsDatabasePersistent)
                {
                    var tokenIdResult = await _svcUserToken.GetId(userId, model.Token, _ClientIpAddress, _ClientUserAgent);

                    if (tokenIdResult > 0)
                    {
                        model.UserTokenId = tokenIdResult;
                    }
                    else
                    {
                        isValidToken = false;
                    }
                }
                else
                {
                    if (_JwtSettings.IsUseIpAddressValidation && !validatedUser.Claims.Any(c => c.Type == AppClaimTypes.IpAddress && c.Value == _ClientIpAddress))
                    {
                        isValidToken = false;
                    }
                    if (_JwtSettings.IsUseUserAgentValidation && !validatedUser.Claims.Any(c => c.Type == AppClaimTypes.UserAgent && c.Value == _ClientUserAgent))
                    {
                        isValidToken = false;
                    }
                }

                if (isValidToken)
                {
                    bool isValidRefreshToken = await _svcUserToken.IsValid(userId, model.RefreshToken, _ClientIpAddress, _ClientUserAgent, _JwtSettings.RefreshTokenMaxProvisionCount);

                    if (isValidRefreshToken)
                    {
                        //Verify last action date from client in order to support sliding expiration
                        if (model.LastActionDate.HasValue)
                        {
                            if (model.LastActionDate.Value >= validatedToken.ValidTo)
                            {
                                //Invalid LastActionDate found
                                model.LastActionDate = null;
                            }
                            else if (model.LastActionDate.Value < validatedToken.ValidTo.AddMinutes(-_JwtSettings.ExpiresInMinutes))
                            {
                                //Session Expired
                                if (_JwtSettings.IsDatabasePersistent)
                                {
                                    await Task.WhenAll(
                                        _svcUserToken.Delete(model.UserTokenId.Value),
                                        _svcUserToken.DeleteByToken(userId, model.RefreshToken));
                                }
                                else
                                {
                                    await _svcUserToken.DeleteByToken(userId, model.RefreshToken);
                                }

                                return result;
                            }
                        }

                        LoginProviderType type = (LoginProviderType)Convert.ToInt32(validatedUser.Claims.FirstOrDefault(c => c.Type == AppClaimTypes.LoginProviderType)?.Value);

                        return await SignInAsync(validatedUser.GetUsername(), type, model);
                    }
                }
            }

            return result;
        }

        public async Task<IEnumerable<Claim>> GenerateClaimsForUser(User userData, LoginProviderType type)
        {
            List<Claim> values = new List<Claim>();

            var userClaims = await _svcUserClaim.GetAllByUserId(userData.Id, ClaimTypes.Role);

            if (userClaims != null && userClaims.Any())
            {
                values = new List<Claim> {
                    new Claim(ClaimTypes.NameIdentifier, userData.Id.ToString()),
                    new Claim(ClaimTypes.Name, userData.UserName),
                    new Claim(ClaimTypes.Email, userData.Email ?? ""),
                    new Claim(ClaimTypes.OtherPhone, userData.Phonenumber ?? ""),
                    new Claim(ClaimTypes.GivenName, userData.FirstName ?? ""),
                    new Claim(AppClaimTypes.MiddleInitial, userData.MiddleInitial ?? ""),
                    new Claim(ClaimTypes.Surname, userData.LastName ?? ""),
                    new Claim(AppClaimTypes.DisplayName, userData.DisplayName ?? ""),
                    new Claim(AppClaimTypes.LoginProviderType, ((int)type).ToString())
                };

                if (!_JwtSettings.IsDatabasePersistent)
                {
                    if (_JwtSettings.IsUseIpAddressValidation)
                    {
                        values.Add(new Claim(AppClaimTypes.IpAddress, _ClientIpAddress ?? ""));
                    }

                    if (_JwtSettings.IsUseUserAgentValidation)
                    {
                        values.Add(new Claim(AppClaimTypes.UserAgent, _ClientUserAgent ?? ""));
                    }
                }

                foreach (var uc in userClaims)
                {
                    values.Add(new Claim(ClaimTypes.Role, uc.ClaimValue));
                }
            }

            return values;
        }

        public ClaimsPrincipal ValidateToken(string token, out SecurityToken validatedToken)
        {
            var validationParams = new TokenValidationParameters
            {
                AuthenticationType = JwtBearerDefaults.AuthenticationScheme,
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                RequireExpirationTime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = _JwtSettings.ValidIssuer,
                ValidAudience = _JwtSettings.ValidAudience,
                IssuerSigningKey = _JwtSettings.SsSignKey,
                TokenDecryptionKey = _JwtSettings.SsEncryptKey
            };

            return new JwtSecurityTokenHandler().ValidateToken(token, validationParams, out validatedToken);
        }

        public static string GenerateJsonWebToken(IEnumerable<Claim> clientClaims, JwtSettings settings, out DateTime tokenExpirationTimeUtc, DateTime? lastActionDate = null)
        {
            JwtSecurityToken securityToken = settings.OnGenerateJwtSecurityToken(clientClaims, lastActionDate);

            tokenExpirationTimeUtc = securityToken.ValidTo;

            return new JwtSecurityTokenHandler().WriteToken(securityToken);
        }

        public static string GenerateRefreshToken()
        {

            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }

        }
    }

    public class RefreshTokenModel
    {
        [Editable(false)]
        public int? UserTokenId { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        public string RefreshToken { get; set; }

        public DateTime? LastActionDate { get; set; }
    }

    public class SignInSettings
    {
        public bool IsApprovalRequired { get; set; }
        public bool IsAutoRegister { get; set; }
        public bool IsLockoutEnabled { get; set; }
        public int MaxAccessFailCount { get; set; }
        public int LockoutTimespanInMinutes { get; set; }
    }

    public class JwtSettings
    {
        public int ExpiresInMinutes { get; set; }
        public bool IsDatabasePersistent { get; set; }
        public bool IsUseRefreshTokens { get; set; }
        public int RefreshTokenMaxProvisionCount { get; set; }
        public bool IsUseIpAddressValidation { get; set; }
        public bool IsUseUserAgentValidation { get; set; }
        public string ValidIssuer { get; set; }
        public string ValidAudience { get; set; }
        public SymmetricSecurityKey SsSignKey { get; private set; }
        public SymmetricSecurityKey SsEncryptKey { get; private set; }


        private string _SignKey;

        public string SignKey
        {
            get { return _SignKey; }
            set
            {
                SsSignKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(value));
                _SignKey = value;
            }
        }


        private string _EncryptKey;

        public string EncryptKey
        {
            get { return _EncryptKey; }
            set
            {
                SsEncryptKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(value));
                _EncryptKey = value;
            }
        }


        public Func<IEnumerable<Claim>, DateTime?, JwtSecurityToken> OnGenerateJwtSecurityToken { get; set; }
    }

    public class SignInResult
    {
        public SignInStatus Status { get; set; }

        public DateTime? LockoutEndDate { get; set; }

        /// <summary>
        /// These claims will be public to the client. DO NOT INSERT SENSITIVE INFORMATION INTO THIS PROPERTY!
        /// </summary>
        public IEnumerable<SignInClaim> Claims { get; set; }

        public string Token { get; set; }

        public string RefreshToken { get; set; }

        public DateTime? TokenExpirationTime { get; set; }

        public DateTime? RefreshTokenExpirationTime { get; set; }
    }

    public class SignInClaim
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }

    public class AuthenticationSettings
    {
        public bool IsFormsEnabled { get; set; }
        public bool IsWindowsEnabled { get; set; }
        public bool IsPkiEnabled { get; set; }
        public bool IsAutoAuthenticate { get; set; }
        public bool IsExternalClient { get; set; }
        public string XsrfToken { get; set; }

        [JsonIgnore]
        public List<string> ExternalIpAddresses { get; set; }

        [JsonIgnore]
        public List<string> InternalIpAddresses { get; set; }
    }
}
