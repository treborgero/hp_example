﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using HP.BLL.Security.Middleware.CertificateAuthentication;

namespace Microsoft.AspNetCore.Authentication
{
    public static class CertificateAuthenticationExtensions
    {
        public static AuthenticationBuilder AddCertificateAuthentication(this AuthenticationBuilder builder, Action<CertficateAuthenticationOptions> configureOptions)
        {
            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<IPostConfigureOptions<CertficateAuthenticationOptions>, CertificateAuthenticationPostConfigureOptions>());
            return builder.AddScheme<CertficateAuthenticationOptions, CertificateAuthenticationHandler>(CertificateAuthenticationDefaults.AuthenticationScheme, "Certificate Authentication", configureOptions);
        }
        public static AuthenticationBuilder AddCertificateAuthentication(this AuthenticationBuilder builder)
        {
            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<IPostConfigureOptions<CertficateAuthenticationOptions>, CertificateAuthenticationPostConfigureOptions>());
            return builder.AddScheme<CertficateAuthenticationOptions, CertificateAuthenticationHandler>(CertificateAuthenticationDefaults.AuthenticationScheme, "Certificate Authentication", options => { });
        }
    }
}
