﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HP.BLL.Security.Middleware.WindowsAuthentication
{
    public static class WindowsAuthenticationDefaults
    {
        /// <summary>
        /// Default value for AuthenticationScheme property in the CertificateAuthenticationOptions
        /// </summary>
        public const string AuthenticationScheme = "WindowsMiddleware";
    }
}
