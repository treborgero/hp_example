﻿using System;

namespace HP.BLL
{
    public class Globals
    {
        public static void SetOnErrorEvent(Action<Exception> onError)
        {
            ExceptionExtensions.OnError = onError;
        }

        public static Action<Exception> GetOnErrorEvent()
        {
            return ExceptionExtensions.OnError;
        }

        public static DateTime FromUnixTimeStamp(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
