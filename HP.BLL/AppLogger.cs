﻿using HP.DAL.Context.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Claims;

namespace HP.BLL
{
    public interface IAppLogger
    {
        void Log(Exception ex);
        void LogByStatusCode(Exception ex, int statusCode);
    }

    public class AppLogger : IAppLogger
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private string _userInfo;
        private int? _statusCode;


        public AppLogger(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }


        public async void Log(Exception ex)
        {

            try
            {
                if (_httpContextAccessor.HttpContext.User != null)
                {
                    string displayName = _httpContextAccessor.HttpContext.User.GetDisplayName(true);
                    string email = _httpContextAccessor.HttpContext.User.GetEmail(true);

                    _userInfo = _httpContextAccessor.HttpContext.User.GetUsername(true)
                        + (!string.IsNullOrWhiteSpace(displayName) ? '\n' + displayName : "")
                        + (!string.IsNullOrWhiteSpace(email) ? '\n' + email : "");
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("User: " + (_userInfo ?? "N/A") + " Exception: " + e.ToString());
#endif
            }


            try
            {
                if (!_statusCode.HasValue)
                {
                    _statusCode = _httpContextAccessor.HttpContext.Response?.StatusCode ?? 0;
                }


                await _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<IErrorRepository>().Insert(new HP.DAL.Context.Entities.Error
                {
                    ErrorId = Guid.NewGuid().ToString(),
                    Host = Environment.MachineName,
                    Application = "TMN",
                    User = _userInfo ?? "",
                    Type = ex.GetType().ToString(),
                    Source = ex.Source ?? "",
                    Message = ex.Message ?? "",
                    StatusCode = _statusCode ?? 0,
                    TimeUtc = DateTime.UtcNow,
                    AllXml = ex.SerializeToXml()
                }, "Sequence");
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("User: " + (_userInfo ?? "N/A") + " Exception: " + e.ToString());
#endif
            }


            _statusCode = null;
        }


        public void LogByStatusCode(Exception ex, int statusCode)
        {
            _statusCode = statusCode;

            Log(ex);
        }
    }
}
