﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HP.DAL.Extensions
{
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Converts the enumerable data to a <see cref="DataTable"/>.
        /// </summary>
        /// <typeparam name="T">The element type of the enumerable.</typeparam>
        /// <param name="data">The <see cref="System.Collections.Generic.IEnumerable{T}"/> data.</param>
        /// <param name="isUseColumnAttributeNames">Set to false to not use the <see cref="System.ComponentModel.DataAnnotations.Schema.ColumnAttribute"/> for names. (Optional)</param>
        /// <param name="tableName">The table name to use for the <see cref="System.Data.DataTable"/>. (Optional)</param>
        /// <returns>The <see cref="DataTable"/> created from the data.</returns>        
        public static DataTable ToDataTable<T>(this IEnumerable<T> data, bool isUseColumnAttributeNames = true, string tableName = null)
            where T : new()
        {
            DataTable dt = string.IsNullOrWhiteSpace(tableName) ? new DataTable() : new DataTable(tableName);


            T obj = default(T);
            Type tType = obj == null ? typeof(T) : obj.GetType();


            var props = tType.GetProperties();


            if (isUseColumnAttributeNames)
            {
                foreach (var item in props)
                {
                    dt.Columns.Add(item.GetColumnAttributeName(), item.PropertyType);
                }
            }
            else
            {
                foreach (var item in props)
                {
                    dt.Columns.Add(item.Name, item.PropertyType);
                }
            }


            if (dt.Columns.Count > 0)
                foreach (T item in data)
                {
                    DataRow row = dt.NewRow();

                    for (int i = 0; i < props.Length; i++)
                    {
                        row[i] = props[i].GetValue(item);
                    }

                    dt.Rows.Add(row);
                }


            return dt;
        }
    }
}
