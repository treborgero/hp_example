﻿using System.Linq;
using System.Security.Principal;
using HP.BLL.Common;

namespace System.Security.Claims
{
    public static class PrincipalExtensions
    {
        public static int GetId(this IPrincipal principal)
        {
            int toReturn = 0;

            if (principal.Identity != null)
            {
                try
                {
                    var identity = principal.Identity as ClaimsIdentity;

                    toReturn = Convert.ToInt32(identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }

            return toReturn;
        }

        public static string GetNameIdentifier(this IPrincipal principal)
        {
            string toReturn = "";

            if (principal.Identity != null)
            {
                try
                {
                    var identity = principal.Identity as ClaimsIdentity;

                    toReturn = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }

            return toReturn;
        }

        public static string GetUsername(this IPrincipal principal, bool isSkipLogging = false)
        {
            string toReturn = "";

            if (principal.Identity != null)
            {
                try
                {
                    var identity = principal.Identity as ClaimsIdentity;

                    toReturn = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                }
                catch (Exception ex)
                {
                    if (!isSkipLogging)
                    {
                        ex.Log();
                    }
                }
            }

            return toReturn;
        }

        public static string GetEmail(this IPrincipal principal, bool isSkipLogging = false)
        {
            string toReturn = "";

            if (principal.Identity != null)
            {
                try
                {
                    var identity = principal.Identity as ClaimsIdentity;

                    toReturn = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email).Value;
                }
                catch (Exception ex)
                {
                    if (!isSkipLogging)
                    {
                        ex.Log();
                    }
                }
            }

            return toReturn;
        }

        public static string GetFirstName(this IPrincipal principal)
        {
            string toReturn = "";

            if (principal.Identity != null)
            {
                try
                {
                    var identity = principal.Identity as ClaimsIdentity;

                    toReturn = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.GivenName).Value;
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }

            return toReturn;
        }

        public static string GetLastName(this IPrincipal principal)
        {
            string toReturn = "";

            if (principal.Identity != null)
            {
                try
                {
                    var identity = principal.Identity as ClaimsIdentity;

                    toReturn = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Surname).Value;
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }

            return toReturn;
        }

        public static string GetMiddleInitial(this IPrincipal principal)
        {
            string toReturn = "";

            if (principal.Identity != null)
            {
                try
                {
                    var identity = principal.Identity as ClaimsIdentity;

                    toReturn = identity.Claims.FirstOrDefault(c => c.Type == AppClaimTypes.MiddleInitial).Value;
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }

            return toReturn;
        }

        public static bool IsAdmin(this IPrincipal principal)
        {
            bool toReturn = false;

            if (principal.Identity != null)
            {
                try
                {
                    var identity = principal.Identity as ClaimsIdentity;

                    toReturn = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value == "Administrator" ? true : false;
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }

            return toReturn;
        }

        public static string GetDisplayName(this IPrincipal principal, bool isSkipLogging = false)
        {
            string toReturn = "";

            if (principal.Identity != null)
            {
                try
                {
                    var identity = principal.Identity as ClaimsIdentity;

                    toReturn = identity.Claims.FirstOrDefault(c => c.Type == AppClaimTypes.DisplayName).Value;
                }
                catch (Exception ex)
                {
                    if (!isSkipLogging)
                    {
                        ex.Log();
                    }
                }
            }

            return toReturn;
        }

        public static ClaimsIdentity GetClaimsIdentity(this IPrincipal principal)
        {
            ClaimsIdentity toReturn = null;

            if (principal.Identity != null)
            {
                try
                {
                    toReturn = principal.Identity as ClaimsIdentity;
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }

            return toReturn;
        }
    }
}
