﻿using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace System
{
    public static class ExceptionExtensions
    {
        public static Action<Exception> OnError;

        public static void Log(this Exception ex)
        {
            try
            {
#if DEBUG
                Debug.WriteLine(ex.ToString());
#endif
                OnError?.Invoke(ex);
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine(e.ToString());
#endif
            }
        }

        public static string SerializeToXml(this Exception ex)
        {
            try
            {
                XmlSerializableException toSerialize = new XmlSerializableException(ex);

                XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

                using (StringWriter textWriter = new StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, toSerialize);
                    return textWriter.ToString();
                }
            }
            catch (Exception)
            {
                return "XML serialization failed!";
            }
        }
    }

    [Serializable]
    public class XmlSerializableException
    {
        public string StackTrace { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string SqlStatement { get; set; }
        public XmlSerializableException InnerException { get; set; }
        public int HResult { get; set; }
        public string HelpLink { get; set; }


        public XmlSerializableException()
        {
        }

        public XmlSerializableException(System.Exception ex)
        {
            StackTrace = ex.StackTrace;
            Source = ex.Source;
            Message = ex.Message;
            HResult = ex.HResult;
            HelpLink = ex.HelpLink;

            var prop = ex.GetType().GetProperty("Statement");

            if (prop != null)
            {
                var statement = prop.GetValue(ex);

                if (statement != null)
                {
                    SqlStatement = statement.GetType().GetProperty("SQL")?.GetValue(statement).ToString();
                }
            }

            if (ex.InnerException != null)
            {
                InnerException = new XmlSerializableException(ex.InnerException);
            }
        }
    }
}
