﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HP.BLL.Common.Smtp
{
    public interface ISmtpSender
    {
        bool IsEnabled { get; }

        void Queue(SmtpSenderMessage message);

        Task DequeueAsync(CancellationToken cancellationToken);
    }
}
