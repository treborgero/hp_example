﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HP.BLL.Common.Smtp
{
    public class SmtpSenderHosterService : BackgroundService
    {
        public SmtpSenderHosterService(ISmtpSender smtpSender)
        {
            _smtpSender = smtpSender;
        }

        private readonly ISmtpSender _smtpSender;

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    await _smtpSender.DequeueAsync(stoppingToken);
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }
        }
    }
}
