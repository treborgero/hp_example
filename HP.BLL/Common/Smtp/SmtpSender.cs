﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HP.BLL.Common.Smtp
{
    public class SmtpSender : ISmtpSender
    {
        private readonly SmtpMailServerSettings _settings;

        private readonly ConcurrentQueue<SmtpSenderMessage> MessageQueue;

        private readonly SemaphoreSlim _signal;

        public bool IsEnabled { get { return _settings.IsEnabled; } }

        public SmtpSender(SmtpMailServerSettings settings)
        {
            _settings = settings;

            MessageQueue = new ConcurrentQueue<SmtpSenderMessage>();

            _signal = new SemaphoreSlim(0);
        }

        public void Queue(SmtpSenderMessage message)
        {
            if (IsEnabled)
            {
                if (message == null)
                {
                    throw new ArgumentNullException(nameof(message));
                }

                MessageQueue.Enqueue(message);
                _signal.Release();
            }
        }

        public async Task DequeueAsync(CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);

            if (MessageQueue.TryDequeue(out SmtpSenderMessage message))
            {
                await SendAsync(message, cancellationToken);
            }
        }

        private MimeMessage BuildMessage(string subject, string body, IEnumerable<string> toAddresses, IEnumerable<string> ccAddresses)
        {
            var message = new MimeMessage();

            message.From.Add(new MailboxAddress(_settings.EmailAddress));

            if (toAddresses != null)
            {
                foreach (var to in toAddresses)
                {
                    message.To.Add(new MailboxAddress(to));
                }
            }
            if (ccAddresses != null)
            {
                foreach (var cc in ccAddresses)
                {
                    message.Cc.Add(new MailboxAddress(cc));
                }
            }

            message.Subject = subject;

            message.Body = new TextPart("plain")
            {
                Text = body
            };

            return message;
        }

        private async Task SendAsync(SmtpSenderMessage senderMessage, CancellationToken token = default(CancellationToken))
        {
            MimeMessage message = null;

            try
            {
                if (_settings?.IsEnabled ?? false)
                {
                    message = BuildMessage(senderMessage.Subject, senderMessage.Body, senderMessage.ToAddresses, senderMessage.CcAddresses);

                    using (var client = new SmtpClient())
                    {
                        //Accept all SSL certificates (in case the server supports STARTTLS)
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;


                        await client.ConnectAsync(_settings.Host, _settings.Port, _settings.IsSslImmediatelyOnConnect, token);


                        // Note: only needed if the SMTP server requires authentication
                        if (!string.IsNullOrWhiteSpace(_settings.Username))
                        {
                            client.Authenticate(_settings.Username, _settings.Password, token);
                        }


                        await client.SendAsync(message, token);
                        await client.DisconnectAsync(true, token);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log();
                //TODO: Log email message into Database;
            }

        }

        private void Send(SmtpSenderMessage senderMessage, CancellationToken token = default(CancellationToken))
        {
            MimeMessage message = null;

            try
            {
                if (_settings.IsEnabled)
                {
                    message = BuildMessage(senderMessage.Subject, senderMessage.Body, senderMessage.ToAddresses, senderMessage.CcAddresses);

                    using (var client = new SmtpClient())
                    {
                        //Accept all SSL certificates (in case the server supports STARTTLS)
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;


                        client.Connect(_settings.Host, _settings.Port, _settings.IsSslImmediatelyOnConnect, token);


                        // Note: only needed if the SMTP server requires authentication
                        if (!string.IsNullOrWhiteSpace(_settings.Username))
                        {
                            client.Authenticate(_settings.Username, _settings.Password, token);
                        }


                        client.Send(message, token);
                        client.Disconnect(true, token);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log();
                //TODO: Log email message into Database;
            }

        }
    }

    public class SmtpMailServerSettings
    {
        public bool IsEnabled { get; set; }

        public string Host { get; set; }

        public int Port { get; set; }

        public bool IsSslImmediatelyOnConnect { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string EmailAddress { get; set; }
    }

    public class SmtpSenderMessage
    {
        public string Subject { get; set; }

        public string Body { get; set; }

        public IEnumerable<string> ToAddresses { get; set; }

        public IEnumerable<string> CcAddresses { get; set; }
    }
}
