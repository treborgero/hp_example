﻿using HP.DAL.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HP.DAL.Context
{
    internal static class QueryBuilder
    {
        public static string Insert<T>(object obj, List<string> namesToSkip = null, bool isUseColumnAttributeName = false)
            where T : class
        {
            if (namesToSkip == null)
            {
                namesToSkip = new List<string>();
            }

            Type tType = typeof(T);
            var props = obj.GetType().GetProperties();

            List<string> cols = new List<string>();
            List<string> colValueParams = new List<string>();

            foreach (var item in props)
            {
                if (item.CanRead && !namesToSkip.Contains(item.Name))
                {
                    var tProp = tType.GetProperty(item.Name);

                    if (tProp != null)
                    {
                        string name = tProp.GetColumnAttributeName();

                        cols.Add(name);
                        colValueParams.Add("@" + (isUseColumnAttributeName ? name : item.Name));
                    }
                }
            }


            return @"INSERT INTO " + GetTableName<T>() + @"
                    (
                        " + string.Join(",", cols) + @"
                    )
	                VALUES (
                            " + string.Join(",", colValueParams) + @"
                    );";
        }

        public static string Update<T>(object obj, string whereCondition, List<string> namesToSkip = null, bool isUseColumnAttributeName = false)
            where T : class
        {
            if (namesToSkip == null)
            {
                namesToSkip = new List<string>();
            }

            Type tType = typeof(T);
            var props = obj.GetType().GetProperties();

            List<string> cols = new List<string>();

            foreach (var item in props)
            {
                if (item.CanRead && !namesToSkip.Contains(item.Name))
                {
                    var tProp = tType.GetProperty(item.Name);

                    if (tProp != null)
                    {
                        string name = tProp.GetColumnAttributeName();

                        cols.Add(name + " = @" + (isUseColumnAttributeName ? name : item.Name));
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(whereCondition))
            {
                return @"Update " + GetTableName<T>() + @" SET
                    " + string.Join(",", cols) + ";";
            }
            else
            {
                return @"Update " + GetTableName<T>() + @" SET
                    " + string.Join(",", cols) + @"
	                WHERE " + whereCondition + ";";
            }
        }

        public static string Disable<T>(int id, string defaultKeyName = "id", string defaultDisableColumnName = "is_disabled")
             where T : class
        {
            return "UPDATE " + GetTableName<T>() + " SET " + defaultDisableColumnName + " = 1 WHERE " + defaultKeyName + " = " + id;
        }

        public static string Delete<T>(int id, string defaultKeyName = "id")
             where T : class
        {
            return "Delete from " + GetTableName<T>() + " WHERE " + defaultKeyName + " = " + id;
        }

        public static string GetTableName<T>() where T : class
        {
            Type tType = typeof(T);
            return tType.GetAttributeValue((TableAttribute ta) => ta.Name) ?? tType.Name;
        }
    }
}
