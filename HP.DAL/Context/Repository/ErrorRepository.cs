﻿using Dapper;
using HP.DAL.Context.Entities;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace HP.DAL.Context.Repository
{
    public class ErrorRepository : Repository<Error>, IErrorRepository
    {
        public ErrorRepository(IConfiguration configuration)
            : base(configuration)
        {
        }


        public override Task<IEnumerable<Error>> GetAll(QueryFlags flags = QueryFlags.None)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryAsync<Error>(@"SELECT *
                                                        FROM public.error
                                                        ORDER BY timeutc DESC
                                                        LIMIT 500;");
            }
        }
    }
}
