﻿using HP.DAL.Context.Entities;
using Microsoft.Extensions.Configuration;

namespace HP.DAL.Context.Repository
{
    public class ContactRepository : Repository<Contact>, IContactRepository
    {
        public ContactRepository(IConfiguration configuration)
            : base(configuration)
        {
        }
    }
}
