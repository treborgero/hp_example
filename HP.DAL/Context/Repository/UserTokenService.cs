﻿using Dapper;
using HP.DAL.Context.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Threading.Tasks;

namespace HP.DAL.Context.Repository
{
    public class UserTokenRepository : Repository<UserToken>, IUserTokenRepository
    {
        public UserTokenRepository(IConfiguration configuration)
            : base(configuration)
        {
        }

        public Task<UserToken> Get(int userId, string token)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryFirstOrDefaultAsync<UserToken>(@"
                        select 
                            u.*
                        from admin.user_token u
                        WHERE id_user = @userId AND token = @token",
                new
                {
                    userId,
                    token
                });
            }
        }

        public Task<DateTime?> GetExpirationTime(int userId, string token, string ipAddress, string userAgent, int maxProvisionCount = 16)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryFirstOrDefaultAsync<DateTime?>(@"
                        select 
                            u.date_expiration
                        from admin.user_token u
                        WHERE id_user = @userId 
                        AND token = @token 
                        AND ip_address = @ipAddress 
                        AND user_agent = @userAgent
                        AND refresh_count <= @maxProvisionCount",
                new
                {
                    userId,
                    token,
                    ipAddress,
                    userAgent,
                    maxProvisionCount
                });
            }
        }

        public async Task<bool> IsValidByExpirationTime(int userId, string token, string ipAddress, string userAgent, int minutesOffset = 0, int maxProvisionCount = 16)
        {
            DateTime? expirationTime = await GetExpirationTime(userId, token, ipAddress, userAgent, maxProvisionCount);

            if (expirationTime.HasValue && (expirationTime.Value.AddMinutes(minutesOffset)) > DateTime.UtcNow)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Task<int> GetId(int userId, string token, string ipAddress, string userAgent, int maxProvisionCount = 16)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryFirstOrDefaultAsync<int>(@"
                        select 
                            u.id_user_token
                        from admin.user_token u
                        WHERE id_user = @userId 
                        AND token = @token 
                        AND ip_address = @ipAddress 
                        AND user_agent = @userAgent
                        AND refresh_count <= @maxProvisionCount
                        AND date_expiration > (timezone('UTC', now()))",
                new
                {
                    userId,
                    token,
                    ipAddress,
                    userAgent,
                    maxProvisionCount
                });
            }
        }

        public async Task<bool> IsValid(int userId, string token, string ipAddress, string userAgent, int maxProvisionCount = 16)
        {
            var result = await GetId(userId, token, ipAddress, userAgent, maxProvisionCount);


            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Task<int> DeleteByToken(int userId, string token)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.ExecuteAsync("Delete from admin.user_token WHERE id_user = @userId AND token = @token",
                    new
                    {
                        userId,
                        token
                    });
            }
        }

        public Task<int> DeleteExpired(int userId, int maxProvisionCount)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.ExecuteAsync("Delete from admin.user_token WHERE id_user = @userId AND ((timezone('UTC', now())) >= date_expiration OR refresh_count >= @maxProvisionCount)",
                    new
                    {
                        userId,
                        maxProvisionCount
                    });
            }
        }

        public Task<int> DeleteExpired(int maxProvisionCount)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.ExecuteAsync("Delete from admin.user_token WHERE ((timezone('UTC', now())) >= date_expiration OR refresh_count >= @maxProvisionCount)",
                    new
                    {
                        maxProvisionCount
                    });
            }
        }
    }
}