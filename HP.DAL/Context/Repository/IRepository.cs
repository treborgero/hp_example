﻿using HP.DAL.Context.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HP.DAL.Context.Repository
{
    [Flags]
    public enum QueryFlags
    {
        None = 0,
        Enabled = 1,
        Disabled = 2,
    }

    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAll(QueryFlags flags = QueryFlags.Enabled);

        Task<T> Get(QueryFlags flags = QueryFlags.Enabled);

        Task<T> GetById(int id, QueryFlags flags = QueryFlags.Enabled);

        Task<int> Insert<T2>(T2 entity, string namesToSkipCommaDelimited = "id") where T2 : class;

        Task<int> Update<T2, T3>(T2 entity, string whereCondition, T3 whereConditionParameters)
            where T2 : class
            where T3 : class;

        Task<int> Disable(int id);

        Task<int> Delete(int id);
    }
}
