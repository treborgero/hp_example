﻿using Dapper;
using HP.DAL.Context.Entities;
using HP.DAL.Extensions;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Reflection;
using System.Threading.Tasks;
using KeyAttribute = System.ComponentModel.DataAnnotations.KeyAttribute;
using TableAttribute = System.ComponentModel.DataAnnotations.Schema.TableAttribute;

namespace HP.DAL.Context.Repository
{
    public abstract class Repository<T> : IRepository<T>
        where T : BaseEntity
    {
        protected readonly string _connectionString;
        protected readonly IConfiguration _configuration;
        protected string DefaultKeyName { get; set; }
        protected string DefaultDisableColumnName { get; set; } = "is_disabled";


        public Repository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.PostgreSQL);
        }


        protected IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(_connectionString);
            }
        }

        protected virtual string GetDefaultKeyName()
        {
            if (DefaultKeyName == null)
            {
                var pi = typeof(T).GetProperties();

                foreach (var item in pi)
                {
                    var keyAttribute = item.GetCustomAttribute<KeyAttribute>();

                    if (keyAttribute != null)
                    {
                        DefaultKeyName = item.GetColumnAttributeName();
                        break;
                    }
                }

                if (DefaultKeyName == null)
                    throw new System.Exception("KeyAttribute not found for entity of type: " + typeof(T).Name);
            }

            return DefaultKeyName;
        }


        public virtual Task<IEnumerable<T>> GetAll(QueryFlags flags = QueryFlags.Enabled)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                string sql = "Select * from " + typeof(T).GetAttributeValue((TableAttribute ta) => ta.Name) ?? typeof(T).Name;

                if (flags.HasFlag(QueryFlags.Enabled) && !flags.HasFlag(QueryFlags.Disabled))
                {
                    sql += " where " + DefaultDisableColumnName + " = '0'";
                }
                else if (!flags.HasFlag(QueryFlags.Enabled) && flags.HasFlag(QueryFlags.Disabled))
                {
                    sql += " where " + DefaultDisableColumnName + " = '1'";
                }

                return dbConnection.QueryAsync<T>(sql);
            }
        }

        public virtual Task<T> Get(QueryFlags flags = QueryFlags.Enabled)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                string sql = "Select * from " + typeof(T).GetAttributeValue((TableAttribute ta) => ta.Name) ?? typeof(T).Name;

                if (flags.HasFlag(QueryFlags.Enabled) && !flags.HasFlag(QueryFlags.Disabled))
                {
                    sql += " where " + DefaultDisableColumnName + " = '0'";
                }
                else if (!flags.HasFlag(QueryFlags.Enabled) && flags.HasFlag(QueryFlags.Disabled))
                {
                    sql += " where " + DefaultDisableColumnName + " = '1'";
                }

                return dbConnection.QueryFirstOrDefaultAsync<T>(sql
                   + " fetch first 1 rows only");
            }
        }

        public virtual Task<T> GetById(int id, QueryFlags flags = QueryFlags.Enabled)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                string sql = "Select * from "
                   + typeof(T).GetAttributeValue((TableAttribute ta) => ta.Name) ?? typeof(T).Name
                   + " where " + GetDefaultKeyName() + " = " + id;

                if (flags.HasFlag(QueryFlags.Enabled) && !flags.HasFlag(QueryFlags.Disabled))
                {
                    sql += " AND " + DefaultDisableColumnName + " = '0'";
                }
                else if (!flags.HasFlag(QueryFlags.Enabled) && flags.HasFlag(QueryFlags.Disabled))
                {
                    sql += " AND " + DefaultDisableColumnName + " = '1'";
                }

                return dbConnection.QueryFirstOrDefaultAsync<T>(sql
                   + " fetch first 1 rows only");
            }
        }

        public virtual Task<int> Insert<T2>(T2 entity, string namesToSkipCommaDelimited = "id") where T2 : class
        {
            if (entity == null)
            {
                throw new ArgumentNullException("The entity parameter cannot be null.");
            }

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                List<string> listNamesToSkip = string.IsNullOrWhiteSpace(namesToSkipCommaDelimited) ? new List<string>() : namesToSkipCommaDelimited.Split(',').AsList();

                return dbConnection.ExecuteAsync(QueryBuilder.Insert<T>(entity, listNamesToSkip), entity);
            }
        }

        public virtual Task<int> Update<T2, T3>(T2 entity, string whereCondition , T3 whereConditionParameters)
            where T2 : class
            where T3 : class
        {
            if (entity == null)
            {
                throw new ArgumentNullException("The entity parameter cannot be null.");
            }

            dynamic parameters = entity;

            if (whereConditionParameters != null)
            {
                parameters = new ExpandoObject();

                var expandoDict = parameters as IDictionary<string, object>;

                PropertyInfo[] entityProps = entity.GetType().GetProperties();

                foreach (var ePropInfo in entityProps)
                {
                    expandoDict.Add(ePropInfo.Name, ePropInfo.GetValue(entity));
                }

                PropertyInfo[] wcProps = whereConditionParameters.GetType().GetProperties();

                foreach (var wcPropInfo in wcProps)
                {
                    if (!expandoDict.ContainsKey(wcPropInfo.Name))
                    {
                        expandoDict.Add(wcPropInfo.Name, wcPropInfo.GetValue(whereConditionParameters));
                    }

                }
            }

            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.ExecuteAsync(QueryBuilder.Update<T>(entity, whereCondition), (parameters as object));
            }
        }

        public virtual Task<int> Disable(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.ExecuteAsync(QueryBuilder.Disable<T>(id, GetDefaultKeyName(), DefaultDisableColumnName));
            }
        }

        public virtual Task<int> Delete(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.ExecuteAsync(QueryBuilder.Delete<T>(id, GetDefaultKeyName()));
            }
        }
    }
}
