﻿using Dapper;
using HP.DAL.Context.Entities;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace HP.DAL.Context.Repository
{
    public class UserClaimRepository : Repository<UserClaim>, IUserClaimRepository
    {
        public UserClaimRepository(IConfiguration configuration)
            : base(configuration)
        {
        }

        public Task<IEnumerable<UserClaim>> GetAllByUserId(int userId, string claimType = null)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryAsync<UserClaim>(@"
                        SELECT id_user_claim, id_user, claim_type, claim_value, date_created, is_disabled
	                    FROM admin.user_claim
                        WHERE id_user = @userId 
                        AND (@claimType IS NULL OR claim_type = @claimType) 
                        AND is_disabled = false",
                    new
                    {
                        userId,
                        claimType
                    });
            }
        }

        public Task<int> DeleteByClaimType(int userId, string claimType)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.ExecuteAsync(
                    @"DELETE FROM admin.user_claim
                      where id_user = @userId 
                      AND claim_type = @claimType",
                    new
                    {
                        userId,
                        claimType
                    });
            }
        }

        public Task<int> Insert(int userId, string claimType, string claimValue)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.ExecuteAsync(
                    @"INSERT INTO admin.user_claim(
	                id_user, claim_type, claim_value)
	                VALUES (@userId, @claimType, @claimValue)",
                    new
                    {
                        userId,
                        claimType,
                        claimValue
                    });
            }
        }
    }
}
