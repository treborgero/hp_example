﻿using HP.DAL.Context.Entities;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HP.DAL.Context.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByLogin(string userName, string pwd);

        Task<User> GetByUserName(string userName);

        Task<int> UpdateLockoutInfo(User userToUpdate);

        Task<UserLoginProvider> GetLoginProvider(int userId, LoginProviderType providerType);

        Task<int> Insert(User userToCreate, IEnumerable<Claim> userClaims, LoginProviderType providerType);

        Task<int> Update<T2>(int id, T2 userToUpdate, string semicolonDelimitedRoles) where T2 : class;

        Task<IEnumerable<string>> GetEmailsByRoles(IEnumerable<string> roles);

        Task<IEnumerable<string>> GetEmailsByRoles(params string[] roles);
    }
}
