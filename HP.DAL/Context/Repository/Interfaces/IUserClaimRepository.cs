﻿using HP.DAL.Context.Entities;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace HP.DAL.Context.Repository
{
    public interface IUserClaimRepository : IRepository<UserClaim>
    {
        Task<IEnumerable<UserClaim>> GetAllByUserId(int userId, string claimType = null);

        Task<int> DeleteByClaimType(int userId, string claimType);

        Task<int> Insert(int userId, string claimType, string claimValue);
    }
}
