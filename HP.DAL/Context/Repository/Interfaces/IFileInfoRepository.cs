﻿using HP.DAL.Context.Entities;

namespace HP.DAL.Context.Repository
{
    public interface IFileInfoRepository : IRepository<FileInfo>
    {
    }
}
