﻿using HP.DAL.Context.Entities;

namespace HP.DAL.Context.Repository
{
    public interface IUserLoginRepository : IRepository<UserLogin>
    {
    }
}