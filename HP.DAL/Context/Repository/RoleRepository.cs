﻿using HP.DAL.Context.Entities;
using Microsoft.Extensions.Configuration;

namespace HP.DAL.Context.Repository
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(IConfiguration configuration)
            : base(configuration)
        {
        }
    }
}
