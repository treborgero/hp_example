﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HP.DAL.Context.Entities
{
    [Table("admin.user_login")]
    public class UserLogin : BaseEntity
    {
        [Key]
        [Column("id_user_login")]
        public int Id { get; set; }

        [Required]
        [Column("id_user")]
        public int UserId { get; set; }

        [Required]
        [Column("id_login_status")]
        public int LoginStatusId { get; set; }

        [Required]
        [Column("id_user_login_provider")]
        public int UserLoginProviderId { get; set; }

        [Required]
        [Column("ip_address")]
        public string IpAddress { get; set; }

        [Required]
        [Column("timestamp")]
        public DateTime TimeStamp { get; set; }
    }
}
