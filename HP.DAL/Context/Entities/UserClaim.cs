﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HP.DAL.Context.Entities
{
    [Table("admin.user_claim")]
    public class UserClaim : BaseEntity
    {
        [Key]
        [Column("id_user_claim")]
        public int Id { get; set; }

        [Required]
        [Column("id_user")]
        public int UserId { get; set; }

        [Required]
        [Column("claim_type")]
        public string ClaimType { get; set; }

        [Required]
        [Column("claim_value")]
        public string ClaimValue { get; set; }

        [Required]
        [Column("date_created")]
        public DateTime CreatedDate { get; set; }

        [Required]
        [Column("is_disabled")]
        public bool IsDisabled { get; set; }
    }
}
