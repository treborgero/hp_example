﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HP.DAL.Context.Entities
{
    [Table("admin.user_token")]
    public class UserToken : BaseEntity
    {
        [Key]
        [Column("id_user_token")]
        public int Id { get; set; }

        [Required]
        [Column("id_user")]
        public int UserId { get; set; }

        [Required]
        [Column("ip_address")]
        public string IpAddress { get; set; }

        [Required]
        [Column("user_agent")]
        public string UserAgent { get; set; }

        [Required]
        [Column("token")]
        public string Token { get; set; }

        [Required]
        [Column("refresh_count")]
        public int RefreshCount { get; set; }

        [Required]
        [Column("date_created")]
        public DateTime CreatedDate { get; set; }

        [Column("date_updated")]
        public DateTime UpdatedDate { get; set; }

        [Column("date_expiration")]
        public DateTime ExpirationDate { get; set; }

        [Required]
        [Column("token_type")]
        public UserTokenType TokenType { get; set; }
    }

    public enum UserTokenType
    {
        Token = 1,
        RefreshToken = 2
    }
}
