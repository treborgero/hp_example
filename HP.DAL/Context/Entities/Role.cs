﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HP.DAL.Context.Entities
{
    [Table("admin.role")]
    public partial class Role : BaseEntity
    {
        [Key]
        [Column("id_role")]
        public int Id { get; set; }

        [Required]
        [Column("name")]
        [StringLength(50)]
        public string Name { get; set; }

        [Column("is_disabled")]
        public bool IsDisabled { get; set; }
    }
}
