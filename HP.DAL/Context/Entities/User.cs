﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HP.DAL.Context.Entities
{
    [Table("admin.user")]
    public partial class User : BaseEntity
    {
        [Key]
        [Column("id_user")]
        public int Id { get; set; }



        [Required]
        [Column("username")]
        [StringLength(256)]
        public string UserName { get; set; }

        [Column("password")]
        [JsonIgnore]
        public string Password { get; set; }

        [Required]
        [Column("name_first")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Column("name_middle_initial")]
        [StringLength(1)]
        public string MiddleInitial { get; set; }

        [Required]
        [Column("name_last")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [Column("title")]
        [StringLength(20)]
        public string Title { get; set; }

        [Column("name_display")]
        [StringLength(101)]
        public string DisplayName { get; set; }


        [Required]
        [EmailAddress]
        [Column("email")]
        [StringLength(256)]
        public string Email { get; set; }

        [Phone]
        [Column("phone")]
        [StringLength(50)]
        public string Phonenumber { get; set; }

        [Required]
        [Column("is_verified")]
        public bool IsVerified { get; set; }

        [Column("date_lockout_end")]
        public DateTime? LockoutEndDate { get; set; }

        [Required]
        [Column("failed_login_count")]
        public int FailedLoginCount { get; set; }

        public bool IsLocked
        {
            get
            {
                return this.LockoutEndDate.HasValue && this.LockoutEndDate.Value > DateTime.Now;
            }
        }


        [Required]
        [Column("date_created")]
        public DateTime CreatedDate { get; set; }

        [Column("disabled_by")]
        public int? DisabledBy { get; set; }

        [Column("date_disabled")]
        public DateTime? DisabledDate { get; set; }

        [Required]
        [Column("is_disabled")]
        public bool IsDisabled { get; set; }

        [Column("is_approved")]
        public bool? IsApproved { get; set; }

        [Column("date_approved")]
        public DateTime? ApprovedDate { get; set; }

        [Column("approved_by")]
        public int? ApprovedBy { get; set; }

        /// <summary>
        /// Semicolon delimited
        /// </summary>
        [Column("roles")]
        public string Roles { get; set; }
    }
}
