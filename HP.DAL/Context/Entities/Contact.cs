﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HP.DAL.Context.Entities
{
    [Table("admin.contact")]
    public partial class Contact: BaseEntity
    {
        [Key]
        [Column("id_user")]
        public int Id { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("phone")]
        public string Phone { get; set; }

        [Column("is_disabled")]
        public bool IsDisabled { get; set; }
    }

}
