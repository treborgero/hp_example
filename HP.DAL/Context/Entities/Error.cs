﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HP.DAL.Context.Entities
{
    [Table("error")]
    public partial class Error : BaseEntity
    {
        [Key]
        [Column("errorid")]
        public string ErrorId { get; set; }

        [Column("application")]
        [StringLength(60)]
        public string Application { get; set; }

        [Column("host")]
        [StringLength(50)]
        public string Host { get; set; }

        [Column("type")]
        [StringLength(100)]
        public string Type { get; set; }

        [Column("source")]
        [StringLength(60)]
        public string Source { get; set; }

        [Column("message")]
        [StringLength(500)]
        public string Message { get; set; }

        [Column("user_info")]
        public string User { get; set; }

        [Column("statuscode")]
        public int StatusCode { get; set; }

        [Column("timeutc")]
        public DateTime TimeUtc { get; set; }

        [Column("sequence")]
        public int? Sequence { get; set; }

        [Column("allxml")]
        public string AllXml { get; set; }
    }
}
