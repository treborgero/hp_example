﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HP.DAL.Context.Entities
{
    public enum FileCategoryType
    {
        A,
        B,
        C,
        D
    }

    public enum FileQueryType
    {
        All,
        Document,
        Image,
    }

    [Table("file")]
    public partial class FileInfo : BaseEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("key")]
        public int Key { get; set; }

        [Column("key_name")]
        public string KeyName { get; set; }

        [Column("id_file_category")]
        public FileCategoryType FileCategoryId { get; set; }

        [NotMapped]
        public string FileCategoryName
        {
            get
            {
                return Enum.GetName(typeof(FileCategoryType), this.FileCategoryId);
            }
        }

        [Column("name")]
        public string Name { get; set; }

        [Column("is_image")]
        public bool IsImage { get; set; }

        [Column("created_date")]
        public DateTime CreatedDate { get; set; }

        [Column("modified_date")]
        public DateTime ModifiedDate { get; set; }

        [Column("modified_by")]
        public int ModifiedBy { get; set; }

        [Column("created_by")]
        public int CreatedBy { get; set; }

        [Column("is_disabled")]
        public bool IsDisabled { get; set; }
    }
}
